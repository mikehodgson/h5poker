var express = require('express'),
  morgan  = require('morgan'),
  cookieParser = require('cookie-parser'),
  session = require('express-session'),
  app     = express(),
  server  = require('http').createServer(app),
  io      = require('socket.io').listen(server);

var players = [];
var globals = {
  max_players : 9, 
  current_players : 0,
  next_player: 1
};

app.locals.pretty = true;

app.set('view engine', 'jade');

app.use(morgan('dev'));
app.use(cookieParser());
app.use(session({secret: "askj1298719^%&^^", key: 'sid', cookie: {secure: true}}));
app.use(require('less-middleware')(__dirname + '/public' ));
app.use(express.static(__dirname + '/public'));

server.listen(3000);

app.get('/', function(req, res) {
  res.render('index', {title: 'home.'});
});

io.sockets.on('connection', function(socket) {
  socket.on('connected', function(data) {
    if (globals.current_players < globals.max_players) {
      players.push({name: data.name, chips: 1500, position: globals.next_player});
      socket.broadcast.emit('user connect', {players: players});
      socket.emit('user connect', {players: players});
      globals.current_players++;
      globals.next_player++;
    } else {
      socket.emit('user denied', {players: players});
    }
  });
});

