var socket = io.connect();

function refresh_players(players) {
	for (var i=0; i < players.length; i++) {
		$('#player-' + players[i].position + ' .player-name').html(players[i].name + '<span class="stack"></span>');
		$('#player-' + players[i].position + ' .player-name .stack').text(players[i].chips);
	}
}

socket.on('user connect', function(data) {
	var players = data.players;
	refresh_players(players);
});

socket.on('user denied', function(data) {
	var players = data.players;
	refresh_players(players);
	alert('Sorry, this game is full!');
});

$(document).ready(function() {
	var width = $('.table-container').width();
	$('.table-container').height(width * 0.60);
    socket.emit('connected', {name: prompt('Please enter your name.')});
});

$(window).on('resize', function() {
	var width = $('.table-container').width();
	$('.table-container').height(width * 0.60);
});

